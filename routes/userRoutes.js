const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

// Check Email
router.post("/checkEmail", (req, res) => {

  // .then method uses the result from the controller function and sends it back to the frontend application via res.send method
  userController.checkEmailExists(req.body).then(resultFromController => res.send (resultFromController));
});

// User registration
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// User authentication

router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Non-admin User checkout (Create Order)

router.post("/checkOut", auth.verify, (req, res) => {
  let user = auth.decode(req.headers.authorization);

  let product = {
    userId: user.id,
    productId: req.body.productId,
    //changed to quantity instead of total amount
    quantity: req.body.quantity,
  };

  if (user.isAdmin === false) {
    userController
      .productOrder(product)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    return res.send(`You are not allowed to do this transaction.`);
  }
});


// Retrieve User Details

router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.getDetails({userId: userData.id}).then(resultFromController => res.send(resultFromController))
});

// Set user as admin (Admin only)

router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
	data = auth.decode(req.headers.authorization);
	if (data.isAdmin){
	userController.setAdmin(req.params, req.body).then(resultFromController => res.send(resultFromController));

	} else {
		res.send(`Only admin accounts are authorized to process this request.`);
	}
});


module.exports = router;