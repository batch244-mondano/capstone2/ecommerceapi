const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

// Create Product (Admin only)
router.post("/newProduct", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if (data.isAdmin){
	productController.newProduct(data).then(resultFromController => res.send(resultFromController))

	} else {
		res.send(`You are not authorized to add a product.`);
	}
});
// Retrieve all products
router.get("/allProducts", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

// Retrieve all active products

router.get("/allActive", (req,res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Retrieve single product

router.get("/:productId", (req,res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});


// Update Product information (Admin only)
router.put("/:productId", auth.verify, (req, res) => {
	data = auth.decode(req.headers.authorization);
	if (data.isAdmin){
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));

	} else {
		res.send(false);
	}
});


// Archive Product (Admin only)
router.patch("/:productId/archive", auth.verify, (req, res) =>{
	data = auth.decode(req.headers.authorization);
	if(data.isAdmin){
	productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
});








module.exports = router;