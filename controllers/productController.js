const Product = require("../models/Product");

// Add a product
module.exports.newProduct = (data) => {

		let addProduct = new Product ({
		name: data.product.name,
		description: data.product.description,
		price: data.product.price,
	});

	return addProduct.save().then((product, error) =>{

		if(error){
			return false;

		} else {
			return true;
		}
	})
};

// Get all products
module.exports.getAllProducts = () => {

	return Product.find({}).then(result => {
		return result;
	})
}

// Get all active Product
module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
}

// Get specific product

module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}

// Update Product information (Admin only)
module.exports.updateProduct = (reqParams, reqBody) => {

	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if(error){
			return false;

		} else {
			return true;
		}
	})
};

// Archive Product (Admin only)
module.exports.archiveProduct = (reqParams, reqBody) =>{

	let updateActiveField = {
		isActive: reqBody.isActive
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((course, error) => {

		if(error) {
			return false;
		} else {
			return true;
		}
	})
};

