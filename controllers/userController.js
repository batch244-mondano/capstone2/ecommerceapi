const bcrypt = require("bcrypt");
const auth = require("../auth");
const User = require("../models/User");
const Product = require("../models/Product");

// Check Email
module.exports.checkEmailExists = (reqBody) => {

	return User.find({email : reqBody.email}).then(result => {

		// The find method returns a record if a match is found
		if (result.length > 0) {
			return true;

		// No duplicate email found
		// The user is not yet registered in the database
		} else {
			return false;
		}
	})
};

// User Registration
module.exports.registerUser = (reqBody)=>{
    let newUser = new User ({
        email : reqBody.email,

        // Syntax: bcrypt.hashSync(dataToBeEncrypted), salt)
        password : bcrypt.hashSync(reqBody.password, 10)
    })

    return newUser.save().then((user,error)=>{
        // user registration failed
        if (error){
            return false;

        // User registration successful
        }else{
            return true;
        }

    })
};

// User authentication

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null){
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false;
			}
		}
	})
};

// Non-admin User checkout (Create Order)

module.exports.productOrder = async (data) => {
 
  let isProductUpdated = await Product.findById(data.productId);
  isProductUpdated.orders.push({ orderId: data.userId });

  let isUserUpdated = await User.findById(data.userId);

  let totalAmount = isProductUpdated.price * data.quantity;
  
  let orderDetails = {
    products: {
      productName: isProductUpdated.name,
      quantity: data.quantity,
    },
    totalAmount: totalAmount,
  };
  
  isUserUpdated.orders.push(orderDetails);

  if (isUserUpdated && isProductUpdated) {
    await isProductUpdated.save();
    await isUserUpdated.save();
    return true;
  } else {
    return false;
  }
};




// Retrieve User Details

module.exports.getDetails = (reqBody) => {

	return User.findById(reqBody.userId).then((result,error) => {
		if(error) {
			console.log(error);
			return false;
		} else {
			result.password = "";
			return result;
		}
	});
};

// Set user as admin (Admin only)

module.exports.setAdmin = (reqParams, reqBody) => {

	let makeAdmin = {
		isAdmin: reqBody.isAdmin
	}

	return User.findByIdAndUpdate(reqParams.userId, makeAdmin).then((admin, error) => {
		if(error){
			return false;

		// if course is updated successfully
		} else {
			return true;
		}
	})
};